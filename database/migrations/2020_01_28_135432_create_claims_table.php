<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nro_cliente');
            $table->string('apellido_nombre');
            $table->string('email');
            $table->string('telefono');
            $table->string('calle');
            $table->string('barrio');
            $table->string('localidad');
            $table->string('tipo_reclamo');
            $table->string('empresa');
            $table->string('asunto');
            $table->string('mensaje');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
