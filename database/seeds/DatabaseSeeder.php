<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
/*
        DB::table('users')->insert([
            'name' => 'Dario Ledesma',
            'email' => 'rdledesma1995@gmail.com',
            'password' => bcrypt('dario449997'), // secret
            'remember_token' => str_random(10),
        ]);*/
            
        DB::table('users')->insert([
            'name' => 'Admin Ente',
            'email' => 'contacto@entereguladorsalta.gov.ar',
            'password' => bcrypt('ente449997'), // secret
            'remember_token' => str_random(10),
        ]);
        
    
    }
}
