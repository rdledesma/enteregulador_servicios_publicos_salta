<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'titulo',
        'contenido',
        'imagen',
        'estado'
    ];
}
