<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Law extends Model
{
        protected $table = 'laws';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'titulo',
        'archivo'
    ];
}
