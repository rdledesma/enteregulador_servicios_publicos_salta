<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    use Notifiable;
    protected $table = 'claims';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'nro_cliente',
        'apellido_nombre',
        'telefono',
        'email',
        'calle',
        'barrio',
        'localidad',
        'tipo_reclamo',
        'empresa',
        'asunto',
        'mensaje'
    ];

}
