<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReporteReclamo extends Notification
{
    use Queueable;
    protected $user;
    protected $source;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($source, $user)
    {
        $this->user = $user;
        $this->source = $source;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $reclamo= $this->source;

        

        return (new MailMessage)
        ->subject('Se ha recibido un nuevo reclamo')
            ->line('El reclamo que se realizó está registrado bajo el número de gestión web '.$reclamo->id)
            ->line('Cliente N°: '.$reclamo->nro_cliente)
            ->line('Apellido y Nombre :'.$reclamo->apellido_nombre)
            ->line('Teléfono: '.$reclamo->telefono)
            ->line('Email: '.$reclamo->email)
            ->line('Calle: '.$reclamo->calle)
            ->line('Barrio: '.$reclamo->barrio)
            ->line('Localidad: '.$reclamo->localidad)
            ->line('Tipo de Reclamo: '.$reclamo->tipo_reclamo)
            ->line('Empresa: '.$reclamo->empresa)
            ->line('Asunto: '.$reclamo->asunto)
            ->line('Mensaje: '.$reclamo->mensaje);


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
