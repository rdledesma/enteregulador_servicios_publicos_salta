<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    protected $table = 'resolutions';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'titulo',
        'archivo',
        'year'
    ];
}
