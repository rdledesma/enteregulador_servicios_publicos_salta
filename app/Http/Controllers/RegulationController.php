<?php

namespace App\Http\Controllers;

use App\Regulation;
use Illuminate\Http\Request;

class RegulationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $regulations = Regulation::all();
        return view('regulations.index',compact('regulations'));
    }


    public function admin()
    {

        $regulations = Regulation::all();
        return view('regulations.admin',compact('regulations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("regulations.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'archivo'=>'required',
        ]);


        $archivo = $request->file('archivo');
        $destinationPath =  public_path('res/reglamentos');

        $regulation = new Regulation();

        $regulation->titulo = $request->titulo;
        $regulation->archivo = $regulation->titulo.'.'.$archivo->getClientOriginalExtension();


        $regulation->save();

        $archivo->move($destinationPath, $regulation->titulo.'.'.$archivo->getClientOriginalExtension());
        return redirect('reglamento/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function show(Regulation $regulation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function edit(Regulation $regulation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Regulation $regulation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Regulation  $regulation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $regulation = Regulation::findOrFail($id);
        $regulation->delete();
        return redirect('reglamento/admin');
    }


    public function getFile($id){
        $regulation = Regulation::findOrFail($id);
        return response()->file(public_path('res/reglamentos/'.$regulation->archivo));
    }
}
