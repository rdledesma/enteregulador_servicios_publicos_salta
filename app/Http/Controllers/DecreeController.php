<?php

namespace App\Http\Controllers;

use App\Decree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DecreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$authorities = Authority::all();
        $normatives = Decree::all();
        return view('decrees.index',compact('normatives'));
    }

    public function admin()
    {
        
        $decrees = Decree::all();
        return view('decrees.admin',compact('decrees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("decrees.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'archivo'=>'required',
        ]);


        $archivo = $request->file('archivo');
        $input['imagename'] = $request->titulo.'.'.$archivo->getClientOriginalExtension();
        $destinationPath =  public_path('res/decretos');

        $normative = new Decree();

        $normative->titulo = $request->titulo;
        $normative->archivo = $normative->titulo.'.'.$archivo->getClientOriginalExtension();

        
        $normative->save();

        $archivo->move($destinationPath, $normative->titulo.'.'.$archivo->getClientOriginalExtension());
        return redirect('decreto/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Normative  $normative
     * @return \Illuminate\Http\Response
     */
    public function show(Normative $normative)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Normative  $normative
     * @return \Illuminate\Http\Response
     */
    public function edit(Normative $normative)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Normative  $normative
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Normative $normative)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Normative  $normative
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decree = Decree::findOrFail($id);
        $decree->delete();
        return redirect('decreto/admin');
    }

    public function getFile($id){
        
        $authority = Decree::findOrFail($id);
        return response()->file(public_path('res/decretos/'.$authority->archivo));
    }
}
