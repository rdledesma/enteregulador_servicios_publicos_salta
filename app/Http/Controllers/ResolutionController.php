<?php

namespace App\Http\Controllers;

use App\Resolution;
use Illuminate\Http\Request;

class ResolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resolutions = Resolution::all();
        return view('resolutions.index',compact('resolutions'));
    }

    public function admin()
    {

        $resolutions = Resolution::all();
        return view('resolutions.admin',compact('resolutions'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("resolutions.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'archivo'=>'required',
            'year' => 'required'
        ]);


        $archivo = $request->file('archivo');
        $input['imagename'] = $request->titulo.'.'.$archivo->getClientOriginalExtension();
        $destinationPath =  public_path('res/resoluciones');

        $resolution = new Resolution();

        $resolution->titulo = $request->titulo;
        $resolution->archivo = $resolution->titulo.'.'.$archivo->getClientOriginalExtension();
        $resolution->year = $request->year;

        $resolution->save();

        $archivo->move($destinationPath, $resolution->titulo.'.'.$archivo->getClientOriginalExtension());
        return redirect('resolucion/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resolution  $resolution
     * @return \Illuminate\Http\Response
     */
    public function show(Resolution $resolution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resolution  $resolution
     * @return \Illuminate\Http\Response
     */
    public function edit(Resolution $resolution)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resolution  $resolution
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resolution $resolution)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resolution  $resolution
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resolution = Resolution::findOrFail($id);
        $resolution->delete();
        return redirect('resolucion/admin');
    }

    public function getFile($id){

        $resolution = Resolution::findOrFail($id);
        return response()->file(public_path('res/resoluciones/'.$resolution->archivo));
    }
}
