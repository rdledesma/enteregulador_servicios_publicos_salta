<?php

namespace App\Http\Controllers;

use App\Law;
use Illuminate\Http\Request;

class LawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $laws = Law::all();
        return view('laws.index',compact('laws'));
    }

    public function admin()
    {

        $laws = Law::all();
        return view('laws.admin',compact('laws'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("laws.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'archivo'=>'required',
        ]);


        $archivo = $request->file('archivo');
        $destinationPath =  public_path('res/leyes');

        $law = new Law();

        $law->titulo = $request->titulo;
        $law->archivo = $law->titulo.'.'.$archivo->getClientOriginalExtension();


        $law->save();

        $archivo->move($destinationPath, $law->titulo.'.'.$archivo->getClientOriginalExtension());
        return redirect('ley/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Law  $law
     * @return \Illuminate\Http\Response
     */
    public function show(Law $law)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Law  $law
     * @return \Illuminate\Http\Response
     */
    public function edit(Law $law)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Law  $law
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Law $law)
    {
        //
    }

    public function destroy($id)
    {
        $law = Law::findOrFail($id);
        $law->delete();
        return redirect('ley/admin');
    }


    public function getFile($id){
        $law = Law::findOrFail($id);
        return response()->file(public_path('res/leyes/'.$law->archivo));
    }
}
