<?php

namespace App\Http\Controllers;

use Image;



use App\Authority;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class AuthorityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        /**the authentication middleware here applies to all functions but
         viewForums() and viewForumDetails() and the opposite of this happens
         when you use only()
         */
          //$this->middleware('auth')->except(['get']);
      }

    public function get(){
        $authorities = Authority::all();

        return $authorities;
    }

    public function index()
    {
        
        $authorities = Authority::all();

        return view('authorities.index',compact('authorities'));

    }


    public function admin()
    {
        $authorities = Authority::all();
        return view('authorities.admin',compact('authorities'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        return view("authorities.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|unique:authorities',
            'descripcion'=>'required',
            'cargo'=>'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $image = $request->file('foto');
        $input['imagename'] = $request->name.'.'.$image->getClientOriginalExtension();
        $destinationPath =  public_path('img/authorities');
        
        


        $authority = new Authority();

        $authority->nombre = $request->nombre;
        $authority->descripcion = $request->descripcion;
        $authority->cargo = $request->cargo;
        $authority->foto = '.'.$image->getClientOriginalExtension();

        
        $authority->save();

        $image->move($destinationPath, $authority->id.'.'.$image->getClientOriginalExtension());
        return redirect('autoridad/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authority  $authority
     * @return \Illuminate\Http\Response
     */
    public function show(Authority $authority)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Authority  $authority
     * @return \Illuminate\Http\Response
     */
    public function edit(Authority $authority)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authority  $authority
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Authority $authority)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authority  $authority
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $authority = Authority::findOrFail($id);
        $authority->delete();
        
        return redirect('autoridad/admin');
    }

    public function getAvatar($id)
    {
        $report = Authority::findOrFail($id);
        $foto = "".$report->id."".$report->foto;
        return response()->file(public_path('img/authorities/'.$foto));
    }
}
