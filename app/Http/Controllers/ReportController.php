<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::all();
        return view('reports.index',compact('reports'));
    }

    public function admin()
    {
        $reports = Report::all();
        return view('reports.admin',compact('reports'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'contenido' => 'required'
        ]);


        $archivo = $request->file('imagen');
        $input['imagename'] = $request->titulo.'.'.$archivo->getClientOriginalExtension();
        $destinationPath =  public_path('res/noticias');

        $report = new Report();
        $report->titulo = $request->titulo;
        $report->contenido = $request->contenido;
        $report->imagen = $report->imagen.'.'.$archivo->getClientOriginalExtension();
        $report->save();
        $archivo->move($destinationPath, $report->titulo.'.'.$archivo->getClientOriginalExtension());
        return redirect('noticia/admin');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $report = Report::findOrFail($id);
        
        return view("reports.show",["noticia"=>$report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::findOrFail($id);
        $report->delete();
        return redirect('noticia/admin');
    }

    public function getFile($id){
        
        $report = Report::findOrFail($id);

        $foto = "".$report->titulo."".$report->imagen;
        return response()->file(public_path('res/noticias/'.$foto));
    }
}
