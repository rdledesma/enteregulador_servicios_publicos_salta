<?php

namespace App\Http\Controllers;

use App\Claim;
use App\Notifications\NuevoReclamo;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;
use App\Notifications\ReporteReclamo;
use App\User;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("claims.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nro_cliente'=> 'required',
            'apellido_nombre'  => 'required|string',
            'telefono'=> 'required',
            'email'  => 'required|string|email',
            'calle'  => 'required',
            'barrio'  => 'required',
            'localidad'  => 'required',
            'tipo_reclamo'  => 'required',
            'asunto'  => 'required',
            'mensaje' => 'required'
        
        ]);
        $claim = new Claim($request->all());
        $claim->save();


        
        $claim->notify(new NuevoReclamo($claim));

        
        $users = User::all();

        foreach ($users as $user){
            $user->notify(new ReporteReclamo($claim,$user));
        }
        
        return redirect()->route('reclamo.show', ['claim' => $claim->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function show($claim)
    {
        $claim = Claim::findOrFail($claim);
        return view("claims.show",["claim"=>$claim]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function edit(Claim $claim)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Claim $claim)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function destroy(Claim $claim)
    {
        //
    }
}
