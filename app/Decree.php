<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Decree extends Model
{
    protected $table = 'decrees';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'titulo',
        'archivo'
    ];
}
