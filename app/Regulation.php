<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regulation extends Model
{
    protected $table = 'regulations';

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'titulo',
        'archivo'
    ];
}
