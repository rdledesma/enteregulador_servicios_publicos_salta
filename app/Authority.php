<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Authority extends Model
{
 

    //Atributos que se van a modificar de forma masiva
    protected $fillable = [
        'nombre',
        'cargo',
        'descripcion',
        'foto'
    ];
}
