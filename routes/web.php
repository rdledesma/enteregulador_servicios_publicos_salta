<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Authority;
use App\Decree;
use App\Regulation;
use App\Report;
use App\Resolution;
use App\Law;

Route::get('/', function () {
    $autoridades = Authority::all();
    $noticias = Report::all()->take(4);
    return view('welcome',compact('autoridades','noticias'));
});



Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();


Route::view('/admin', 'admin')->name('admin')->middleware('auth');
Route::get('/noticia/admin', 'ReportController@admin')->name('noticia.admin')->middleware('auth');
Route::get('/autoridad/admin', 'AuthorityController@admin')->name('autoridad.admin')->middleware('auth');
Route::get('/decreto/admin', 'DecreeController@admin')->name('decreto.admin')->middleware('auth');
Route::get('/reglamento/admin', 'RegulationController@admin')->name('reglamento.admin')->middleware('auth');
Route::get('/resolucion/admin', 'ResolutionController@admin')->name('resolucion.admin')->middleware('auth');
Route::get('/ley/admin', 'LawController@admin')->name('ley.admin')->middleware('auth');








Route::view('/contacto', 'contacto')->name('contacto');
Route::view('/subsidio', 'subsidio')->name('subsidio');
Route::view('/informacion', 'informacion')->name('informacion');
Route::view('/factura', 'factura')->name('factura');
Route::view('/educa', 'educa')->name('educa');
Route::view('/calidad', 'calidad')->name('calidad');






Route::get('/normativa', function () {
    $decretos = Decree::all();
    $reglamentos = Regulation::all();
    $resoluciones = Resolution::all();
    $leyes = Law::all();
    return view('normativa',compact('leyes','decretos','reglamentos','resoluciones'));
})->name('normativa');



Route::resource('/reclamo', 'ClaimController');
Route::resource('/autoridad', 'AuthorityController');


Route::get('/reglamento/file/{id}', 'RegulationController@getFile')->name('reglamento.file');
Route::resource('/reglamento', 'RegulationController');

Route::resource('/decreto', 'DecreeController');
Route::get('/decreto/file/{id}', 'DecreeController@getFile')->name('decreto.file');


Route::resource('/resolucion', 'ResolutionController');
Route::get('/resolucion/file/{id}', 'ResolutionController@getFile')->name('resolucion.file');


Route::resource('/ley', 'LawController');
Route::get('/ley/file/{id}', 'LawController@getFile')->name('ley.file');


Route::get('/autoridades', 'AuthorityController@get')->name('autoridades');
Route::get('/autoridad/imagen/{id}', 'AuthorityController@getAvatar')->name('image.displayImage');



Route::resource('/noticia', 'ReportController');

Route::get('/noticia/file/{id}', 'ReportController@getFile')->name('noticia.file');

