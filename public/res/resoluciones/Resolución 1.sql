-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-02-2020 a las 01:03:38
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbenteregulador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authorities`
--

CREATE TABLE `authorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `authorities`
--

INSERT INTO `authorities` (`id`, `nombre`, `cargo`, `foto`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Carlos Humberto Saravia', 'Presidente del Ente Regulador de los Servicios Públicos', '.jpg', 'Abogado', '2020-02-07 23:48:30', '2020-02-07 23:48:30'),
(2, 'Dr. Mariano Ovejero', 'Secretario General del Ente Regulador de los Servicios Públicos', '.jpg', 'Abogado', '2020-02-07 23:59:17', '2020-02-07 23:59:17'),
(3, 'Dr. Fernando Saravia Toledo', 'Miembro del Directorio del Ente Regulador de los Servicios Públicos', '.jpg', 'Abogado', '2020-02-08 00:05:35', '2020-02-08 00:05:35'),
(4, 'Dr. Jeronimo López Fleming', 'Miembro del Directorio del Ente Regulador de los Servicios Públicos', '.jpg', 'Abogado', '2020-02-08 00:06:15', '2020-02-08 00:06:15'),
(5, 'Dr. Daniel Paganetti', 'Miembro del Directorio del Ente Regulador de los Servicios Públicos', '.jpg', 'Abogado', '2020-02-08 00:10:11', '2020-02-08 00:10:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `claims`
--

CREATE TABLE `claims` (
  `id` int(10) UNSIGNED NOT NULL,
  `nro_cliente` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido_nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `barrio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_reclamo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empresa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asunto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensaje` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `claims`
--

INSERT INTO `claims` (`id`, `nro_cliente`, `apellido_nombre`, `email`, `telefono`, `calle`, `barrio`, `localidad`, `tipo_reclamo`, `empresa`, `asunto`, `mensaje`, `created_at`, `updated_at`) VALUES
(1, '1', 'Rubén Darío Ledesma', 'rdledesma_95@hotmail.com', '03875076494', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Salta', 'Queja', 'Dios', 'Me quiero quejar', 'asdasdasdasdasdasdasdas', '2020-02-05 14:12:37', '2020-02-05 14:12:37'),
(2, '1', 'Rubén Darío Ledesma', 'rdledesma_95@hotmail.com', '03875076494', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Salta', 'Consulta', 'Aguas del Norte', 'Me quiero quejar', 'asdadsasdasdasda', '2020-02-06 14:13:52', '2020-02-06 14:13:52'),
(3, '1', 'Rubén Darío Ledesma', 'rdledesma_95@hotmail.com', '03875076494', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Salta', 'Reclamo', 'EDESA', 'Me quiero quejar', 'asdasdasdasdads', '2020-02-10 16:46:44', '2020-02-10 16:46:44'),
(4, '1', 'Rubén Darío Ledesma', 'rdledesma_95@hotmail.com', '03875076494', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Parque Belgrano Etapa 2 Block G dpto 15', 'Salta', 'Reclamo', 'EDESA', 'Me quiero quejar', 'Hola dios?', '2020-02-11 16:40:25', '2020-02-11 16:40:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `decrees`
--

CREATE TABLE `decrees` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_01_28_135432_create_claims_table', 1),
(4, '2020_01_29_095538_create_authorities_table', 1),
(5, '2020_02_02_223025_create_decrees_table', 1),
(6, '2020_02_03_110453_create_regulations_table', 1),
(7, '2020_02_03_115542_create_resolutions_table', 1),
(8, '2020_02_04_094852_create_reports_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regulations`
--

CREATE TABLE `regulations` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenido` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publica',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reports`
--

INSERT INTO `reports` (`id`, `titulo`, `contenido`, `imagen`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'MUNICIPIOS ABRIRÁN OFICINA DEL ENTE PARA ATENCIÓN DE USUARIOS', 'El Ente Regulador de Servicios Públicos inició una serie de acuerdos con municipios de Salta para implementar un trabajo en conjunto que permita al usuario de los servicios de agua y luz realizar trámites en su lugar de origen. \r\nEn el marco de la política implementada por el Gobierno de la Provincia, tendiente a impulsar un trabajo articulado con los municipios, el Organismo firmó convenios de colaboración con diferentes municipalidades para facilitar a los salteños la accesibilidad a las consultas, reclamos y postulaciones a los subsidios. \r\nHasta la fecha, los siguientes municipios ya acordaron abrir una oficina en sus edificios: General Güemes, La Caldera, Pichanal, Aguaray, Isla de Cañas, Apolinario Saravia, Embarcación, Rosario de Lerma, Campo Quijano e Iruya.\r\nEl acuerdo indica que la municipalidad designa agentes y el espacio de atención, mientras que el Organismo brindará la capacitación integral y aporte del soporte técnico para gestionar los trámites de los usuarios.', '.jpg', 'publica', '2020-02-06 15:19:52', '2020-02-06 15:19:52'),
(2, 'INSTITUCIONES BENEMERITAS FUERON SUBSIDIADAS PARA EL USO DE AGUA Y LUZ', 'El Directorio del Ente Regulador de Servicios Públicos estableció esta semana que ingresen nuevas instituciones al régimen de subsidios sobre los servicios de agua y luz.\r\nDe acuerdo a las competencias otorgadas por el marco regulatorio, el Organismo analizó el cumplimiento de los requisitos que presentaron diversas entidades sociales que postularon al subsidio de los servicios. \r\nDe esta forma, las entidades acreditaron que las actividades que desarrollan prestan servicios gratuitos de protección y amparo a la niñez y ancianidad desvalida. Ellos son: \r\nCentro de Jubilados Santa Cecilia: contiene a 900 personas de B° Siglo XXI.\r\nBiblioteca popular Nora Godoy: Brinda el servicio en forma de la zona. Además, organiza actividades y talleres recreativos.\r\nClub Sportivo El Carril: Mediante diferentes disciplinas deportivas contiene a más de 300 jóvenes del lugar.\r\nFundación Minnesota: Contiene y recupera a Jóvenes para una reinserción social. \r\nCasita de Belén: Cuenta con tres sedes y educa integralmente a niños en condición de calle.\r\nLos interesados en averiguar sobre los requisitos para acceder al subsidio para Instituciones Beneméritas puede acudir personalmente a Bartolomé Mitre 1231, llamar al 0800-444-7400 o ingresara a www.entereguladorsalta.gov.ar .', '.jpeg', 'publica', '2020-02-06 15:20:38', '2020-02-06 15:20:38'),
(3, 'REDUCCIÓN TARIFARIA PARA USUARIOS DE ROSARIO DE LERMA.', 'El Directorio del Ente Regulador de Servicios Públicos ordenó a Co.S.A.ySa  efectuar una reducción del 70% en la facturación del servicio sanitario correspondiente a noviembre y diciembre de 2019, para los usuarios del Rosario de Lerma, y que se mantenga hasta tanto acredite la normalización del servicio. \r\nDe acuerdo a los informes elevados por la gerencia interviniente, se corroboró que el municipio padeció deficiencias en la prestación del servicio, durante el período mencionado. \r\nSegún la normativa vigente, Ente Regulador, puede disponer lo necesario para que los servicios de la empresa se presten con los niveles de calidad, protección del medio ambiente y de los recursos naturales, conforme a los caracteres de regularidad, uniformidad, generalidad y obligatoriedad.\r\nEl Organismo de Control dejó sentado que ante la inexistencia de la prestación puede ordenarse la eximición del pago de la tarifa, resguardando la proporcionalidad entre la efectiva prestación y la facturación del servicio. \r\nLa disminución dispuesta se verá reflejada las próximas facturas que reciban los usuarios del municipio.', '.jpg', 'publica', '2020-02-06 17:39:14', '2020-02-06 17:39:14'),
(4, 'EL ENTE REGULADOR DETALLÓ ACCIONES EN ROSARIO DE LERMA POR LA PRESTACIÓN DEL AGUA', 'Una comisión del Ente Regulador de Servicios Públicos visitó hoy la municipalidad de Rosario de Lerma para informar a funcionarios y ediles las acciones que adoptará el Organismo ante los reclamos de los usuarios por la prestación del servicio de agua.\r\nLa reunión fue encabezada por el presidente del EnReSP, Carlos Saravia, y el intendente Enrique Martínez. Allí, el jefe comunal describió los inconvenientes que aquejan a los rosarinos en términos de servicio de agua potable. Del mismo modo, lo propio hicieron concejales y funcionarios municipales. Todos coincidieron que la calidad del agua y la duración de los cortes son los reclamos más frecuentes. \r\nDesde Organismo se propuso firmar de forma inmediata un convenio con el municipio para abrir una oficina del Ente Regulador en el predio municipal donde los usuarios puedan hacer trámites, reclamos y postulación a los subsidios sobre la prestación de agua y luz. \r\nAdemás, personal técnico del Ente puntualizó a los presentes sobre los relevamientos que se hicieron para verificar el servicio en la zona. Los especialistas en calidad del agua les llevaron tranquilidad al indicarles que a partir de las muestras tomadas no se evidenciaron indicadores de contaminación en las redes de distribución de agua.\r\nFinalmente, en la próxima reunión de directorio el Organismo tomará una resolución que de acuerdo a las evidencias recabadas podrían eximir a los usuarios en el pago del servicio total o parcial de la boleta, de acuerdo a lo que indiquen los relevamientos técnicos. \r\nEl intendente definió a la reunión cómo positiva y agregó que: “Agradecemos que el Ente Regulador nos brinde apoyo para que nosotros podamos trabajar para los vecinos de Rosario de Lerma y en conjunto poder llevarles una mejor calidad de vida”', '.jpg', 'publica', '2020-02-06 17:40:14', '2020-02-06 17:40:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resolutions`
--

CREATE TABLE `resolutions` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archivo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dario Ledesma', 'rdledesma1995@gmail.com', '$2y$10$UI45TJlX5p6RfIGHzFzewuqr231TaFZ7uTTn5kD2kvdoeR9LFVS3y', 'uNlImPVpJQ', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `authorities`
--
ALTER TABLE `authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `claims`
--
ALTER TABLE `claims`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `decrees`
--
ALTER TABLE `decrees`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `regulations`
--
ALTER TABLE `regulations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `resolutions`
--
ALTER TABLE `resolutions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `authorities`
--
ALTER TABLE `authorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `claims`
--
ALTER TABLE `claims`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `decrees`
--
ALTER TABLE `decrees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `regulations`
--
ALTER TABLE `regulations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `resolutions`
--
ALTER TABLE `resolutions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
