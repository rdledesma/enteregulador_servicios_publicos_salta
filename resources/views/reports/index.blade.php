@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>
        

          </tr>
        </thead>
        <tbody>
            
            @foreach ($reports as $report)
           
                <tr>
                        
                    
                    <td> <small>{{ \Carbon\Carbon::parse($report->created_at)->format('d/m/Y')}}</small></td>
                    <td> <a href="{{route('noticia.show',$report->id)}}"><h3 class="h3-ente">{{$report->titulo}}</h3></a> </td>
                    <td> <img src="{{route('noticia.file',$report->id)}}" alt="" width="25%"></td>
                </tr>
           
            

            @endforeach
          
                
        </tbody>
      </table>
    </div>
</div>

@endsection
