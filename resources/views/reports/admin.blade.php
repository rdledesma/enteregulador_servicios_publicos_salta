@extends('home')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
      <a href="{{route('noticia.create')}}">Nueva Noticia</a>
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>
            
                <th scope="col">Titulo</th>
                
                <th scope="col">Fecha</th>
                <th scope="col">Acción</th>

          </tr>
        </thead>
        <tbody>
            
            @foreach ($reports as $report)
            <tr>
                
              <td> {{$report->titulo}}</td>
              <td> {{$report->created_at}}</td>

              
            
              
              
              <td>
                <form action="{{action('ReportController@destroy', $report->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
              </td>

            </tr>
           
            

            @endforeach
          
                
        </tbody>
      </table>
    </div>
</div>

</div>

<br><br><br>

<br><br><br>
<br><br><br>

@endsection
