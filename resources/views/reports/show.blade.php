@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col text-center">
        <h2>{{$noticia->titulo}}</h2>
    </div>
</div>

<div class="row-sm text-center">
  <div class="col-sm text-center">
    <img src="{{route('noticia.file',$noticia->id)}}" class="img-thumbnail img-responsive shadow float-center" width="30%"
        data-holder-rendered="true">
  </div>
  <div class="col-sm mt-5">
    <p class="text-justify "> {{$noticia->contenido}} </p>
  </div>
</div>

<div class="row">
  <div class="col text-center">
      <a href="{{route('noticia.index')}}">Ver todas las noticias</a>
  </div>
</div>

@endsection
