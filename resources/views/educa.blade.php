@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col">
        <h3 class="h3-ente">¿Qué es Ente Educa?</h3>
        <p class="p-ente text-justify">
            Ente Educa es un programa educativo del Ente Regulador de los Servicios Públicos. El programa abarca diversas iniciativas sociales orientadas a promover el uso responsable de los servicios públicos de agua y energía eléctrica, a difundir las funciones y competencias del Organismo y los deberes, derechos y obligaciones de los usuarios.
        </p>
        <p class="p-ente text-justify">
            La presencia del Ente en las Escuelas se centra en el convencimiento de que las pautas y los hábitos de consumo establecidos en la temprana edad permiten generar consumidores responsables. Si bien los talleres se desarrollan para diferentes públicos, son los niños y los jóvenes en edad escolar los principales destinatarios, ya que se convierten en agentes multiplicadores de los mensajes por excelencia.

        </p>

        <h3 class="h3-ente">Objetivo</h3>
            <p class="p-ente text-justify">
                El Ente Educa es una iniciativa que apunta a colaborar en la promoción de los valores positivos y comportamientos responsables. La intención es que los niños y los jóvenes comprendan la importancia de los recursos hídricos y eléctricos en el desarrollo de su vida diaria y se conviertan en protagonistas responsables y activos.
            </p>


        <h3 class="h3-ente">Uso Responsable</h3>
        <p class="p-ente">A través del dictado de talleres educativos, se intenta instaurar en la sociedad la importancia que tienen los usuarios en la consecución de soluciones a los problemas que se presentan a diario en la prestación de servicios esenciales como lo son el agua y la luz.
        </p>
        <p class="p-ente">También se organizan charlas con vecinos, entidades intermedias, universidades, como así también se participa en congresos, conferencias y se diseña material educativo siempre bajo la consigna de lograr “ciudadanos participativos y consumidores responsables”.    
        </p>
        

        <h3 class="h3-ente">Dinámica</h3>
        <p class="p-ente">Los talleres para primaria y secundaria tienen una duración de una hora y media. Inician proyectando en formato power point, los conceptos fundamentales sobre el Ente Regulador de los Servicios Públicos y los deberes y derechos de los usuarios, adaptados en cada caso al público correspondiente.
        </p>
        <p>Luego se proyecta un video que ilustra las experiencias de niños de diversas comunidades de la Provincia, con los servicios de agua potable y energía eléctrica, con el objetivo de que los jóvenes conozcan las vivencias de sus pares y proyecten las mismas situaciones en su comunidad.
        </p>
        <p class="p-ente">Al final se entrega una encuesta para que los chicos evalúen a los disertantes, el taller, la temática y expresen sus sentimientos al respecto.
        </p>
        
        <h3 class="h3-ente">Temática</h3>
        <p class="p-ente">Las temáticas de los talleres que ofrece el Ente Regulador son:</p>
        <ul>
            <li class="p-ente">Servicios Públicos – Conceptos Básicos y Caracteres</li>
            <li class="p-ente">Ente Regulador: Misiones y funciones</li>
            <li class="p-ente">Ejercicio de derechos y obligaciones como usuario de luz, cloacas y agua potable</li>
            <li class="p-ente">Procedimientos de reclamos</li>
            <li class="p-ente">Potestades jurisdiccionales del Ente Regulador de los Servicios Públicos</li>
            <li class="p-ente">Subsidios por indigencia</li>
            <li class="p-ente">Ciclo del agua</li>
            <li class="p-ente">Huella Hídrica</li>
            <li class="p-ente">Agua Virtual</li>
            <li class="p-ente">Saneamiento</li>
            <li class="p-ente">Ahorro de agua y energía</li>
            <li class="p-ente">Interpretación y lectura de una factura de luz y agua.</li>
            <li class="p-ente">Micro Medición – ¿Cómo leer los Medidores de Agua Potable y Energía Eléctrica?</li>
            <li class="p-ente">Calidad de Agua Potable, Laboratorio y Análisis Bacteriológicos</li>
            <li class="p-ente">Calidad de Energía Eléctrica – Eficiencia Energética y utilización de focos de bajo consumo</li>
        </ul>

  </div>
</div>
@endsection
