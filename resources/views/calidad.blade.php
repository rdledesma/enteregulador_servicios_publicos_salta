@extends('layouts.app')

@section('content')

<div class="row fondo text-center d-flex justify-content-center" >
    <div class="col text-center">
        <div >
            <img class="imgroja">
        
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col text-center">
        
        <h3 class="h3-ente">CALIDAD</h3>
        <br>
    </div>
</div>
<br>


<div class="row d-flex justify-content-between">
    <div class="col-5">
        <h5 class="h5-ente-rojo">Sistema de Gestión de la Calidad</h5>

        <p class="p-ente">
            Con el fin de cumplir su misión de manera eficiente y mejorar
            continuamente su desempeño, el ENTE REGULADOR DE LOS
            SERVICIOS PÚBLICOS implemetó un Sistema de Gestión de
            Calidad.
        </p>
        <p class="p-ente">
            Para ello se basó en los requisitos de la norma internacional ISO 9001:2015
        </p>
        <p class="p-ente">
            Dicho sistema permite que los procesos de las Áreas certificadas
            estén estandarizados y sean continuamente evaluados para
            detectar desvíos que puedan ser modificados y así mejorar el
            desempeño del Organismo.
        </p>
    </div>

    <div class="col-5">
        <div class="row">
            <div class="col">
                <img src="{{URL::asset('/img/veritas.jpg')}}"  alt="" width="100%;">
            </div>
        </div>
        <div class="row">
            <p class="p-ente">El Sistema de Gestión de Calidad del ENTE REGULADOR DE LOS
                SERVICIOS PÚBLICOS posee el siguiente alcance:
                </p>
            <p class="p-ente">“Atención personalizada y telefónica (Call Center) al usuario,
                otorgamiento de subsidios. Control, fiscalización y regulación del
                servicio de agua potable y saneamiento. Control tarifario de los
                servicios regulados”.
                </p>

                <p class="p-ente-rojo">
                    Certificado de Calidad (OAA_ 9001-2015) CERTIFICATE ARO236352
                </p>
        </div>
    </div>
</div>



@endsection
