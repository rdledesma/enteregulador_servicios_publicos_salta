@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">

                <div class="card"  style="border:none; width: 50%;">
                    <a href="{{route('normativa')}}">
                    <img id="normativa-card" class="card-img-top" src="{{URL::asset('/img/normativa-negativo.png')}}" alt="Card image cap">
                    <div class="card-body text-center">
                        <h5 class="card-title">NORMATIVAS</h5>
                    </div>
                </a>
                  </div>

        </div>
        <div class="col-md-4">

            <div class="card"  style="border:none; width: 50%;">
                <a href="{{route('calidad')}}">
                  <img class="card-img-top"
                    id="calidad-card"
                    src="{{URL::asset('/img/calidad-nagativo.png')}}"
                    alt="Card image cap"

                    >

                <div class="card-body text-center">
                  <h5 class="card-title">CALIDAD</h5>
                </div>
            </a>
              </div>

        </div>
    </div>






</div>


@push('scripts')
<script>
    $( document ).ready(function() {

    $("#normativa-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/normativa-positivo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/normativa-negativo.png')}}");
    }
    });

    $("#calidad-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/calidad-positivo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/calidad-nagativo.png')}}");
    }
    });
  });







</script>
@endpush
@endsection
