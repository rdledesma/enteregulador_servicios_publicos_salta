@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
      <a href="{{route('resolucion.create')}}">Nuevo</a>
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>
            
                <th scope="col">Titulo</th>
                <th scope="col">Año</th>
                <th scope="col">Archivo</th>

                <th scope="col">Acción</th>

          </tr>
        </thead>
        <tbody>
            
            @foreach ($resolutions as $resolution)
            <tr>
                
              <td> {{$resolution->titulo}}</td>
              <td> {{$resolution->year}}</td>
              <td><a href="{{route('resolucion.file',$resolution->id)}}">Descargar</a></td>

              
            
              
              
              <td>
                <form action="{{action('ResolutionController@destroy', $resolution->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
              </td>

            </tr>
           
            

            @endforeach
          
                
        </tbody>
      </table>
    </div>
</div>

</div>


@endsection
