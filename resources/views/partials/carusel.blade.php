<div class="container text-center " style="">
	<div class="row text-center">
		<!-- Carousel -->
    	<div id="carousel-example-generic" data-interval="50000" class="carousel slide text-center" data-ride="carousel" style="width: 1000%;">
			<!-- Indicators -->
			
			<!-- Wrapper for slides -->
			<div class="carousel-inner d-flex justify-content-center">
                @foreach ($authorities as $item)
                @if ($loop->index == 0)
                <div class="item active" style="width: 75%;">
                    <div class="row mt-5">
                        <div class="col-3">
                            <img src="{{route('image.displayImage',$item->id)}}" alt="Imagen no encontrada" width="75%">
                            <p class="mt-5 font-weight-bold" style="color: black;">{{$item->nombre}}</p> 
                            <p class="font-weight-bold" style="color: black; font-size: 140%;">{{$item->cargo}}</p>
                        </div>
                        <div class="col-8  ml-1">
                            <p class="text-justify font-weight-bold " style="color: black; font-size: 150%;">{{$item->descripcion}}</p>
                        </div>
                    </div>
                </div>
                
                @else
                <div class="item" style="width: 75%;">
                    <div class="row mt-5">
                        <div class="col-3">
                            <img src="{{route('image.displayImage',$item->id)}}" alt="Imagen no encontrada" width="75%">
                            <p class="mt-5 font-weight-bold" style="color: black;">{{$item->nombre}}</p> 
                            <p class="font-weight-bold" style="color: black; font-size: 140%;">{{$item->cargo}}</p>
                        </div>
                        <div class="col-8  ml-1">
                            <p class="text-justify font-weight-bold " style="color: black; font-size: 150%;">{{$item->descripcion}}</p>
                        </div>
                    </div>
                </div>
                @endif
                
                @endforeach

			</div>
			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
			</a>
		</div><!-- /carousel -->
	</div>
</div>