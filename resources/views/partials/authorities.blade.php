<div class="row m-3 " >
    @foreach ($autoridades as $autoridad)
    <div class="col-sm mb-5">


        <div class="card mx-auto" style="width: 15rem; border: 0; ">
            <img class="card-img-top"  src="{{route('image.displayImage',$autoridad->id)}}" alt="Imagen no encontrada" width="15%">
            <div class="card-body">
              <h5 class="card-title h5-ente">{{$autoridad->nombre}}</h5>
              <p class="card-text text-muted">{{$autoridad->descripcion}} </p>
              <p class="card-text p-ente">{{$autoridad->cargo}}</p>
            </div>
          </div>
    </div>
    @endforeach

  </div>


</div>
