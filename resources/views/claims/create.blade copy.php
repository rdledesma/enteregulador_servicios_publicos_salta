@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Reclamos</div>

                <div class="card-body">
                <form method="POST" action="{{route('reclamo.store')}}" aria-label="">
                        @csrf

                        <div class="form-group row">
                            <label for="nro_cliente" class="col-md-4 col-form-label text-md-right">Nro de cliente</label>

                            <div class="col-md-6">
                                <input id="nro_cliente" type="text" class="form-control{{ $errors->has('nro_cliente') ? ' is-invalid' : '' }}" name="nro_cliente" value="{{ old('nro_cliente') }}" required autofocus>

                                @if ($errors->has('nro_cliente'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nro_cliente') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="apellido_nombre" class="col-md-4 col-form-label text-md-right">Apellido y Nombre</label>

                            <div class="col-md-6">
                                <input id="apellido_nombre" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="apellido_nombre" value="{{ old('apellido_nombre') }}" required autofocus>

                                @if ($errors->has('apellido_nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('apellido_nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telefono" class="col-md-4 col-form-label text-md-right">Teléfono</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" name="telefono" value="{{ old('telefono') }}" required autofocus>

                                @if ($errors->has('telefono'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="calle" class="col-md-4 col-form-label text-md-right">Calle</label>

                            <div class="col-md-6">
                                <input id="calle" type="text" class="form-control{{ $errors->has('calle') ? ' is-invalid' : '' }}" name="calle" value="{{ old('calle') }}" required autofocus>

                                @if ($errors->has('calle'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('calle') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="barrio" class="col-md-4 col-form-label text-md-right">Barrio</label>

                            <div class="col-md-6">
                                <input id="barrio" type="barrio" class="form-control{{ $errors->has('barrio') ? ' is-invalid' : '' }}" name="barrio" value="{{ old('barrio') }}" required>

                                @if ($errors->has('barrio'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('barrio') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="localidad" class="col-md-4 col-form-label text-md-right">Localidad</label>

                            <div class="col-md-6">
                                <input id="localidad"  class="form-control{{ $errors->has('localidad') ? ' is-invalid' : '' }}" name="localidad" value="{{ old('localidad') }}" required>

                                @if ($errors->has('localidad'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('localidad') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tipo_reclamo" class="col-md-4 col-form-label text-md-right">Tipo de Reclamo</label>

                            <div class="col-md-6">
                                <input id="tipo_reclamo" type="text" class="form-control{{ $errors->has('tipo_reclamo') ? ' is-invalid' : '' }}" name="tipo_reclamo" value="{{ old('tipo_reclamo') }}" required>

                                @if ($errors->has('tipo_reclamo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('tipo_reclamo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                     
                        <div class="form-group row">
                            <label for="empresa" class="col-md-4 col-form-label text-md-right">Empresa</label>

                            <div class="col-md-6">
                                <input id="empresa" type="text" class="form-control{{ $errors->has('empresa') ? ' is-invalid' : '' }}" name="empresa" value="{{ old('empresa') }}" required>

                                @if ($errors->has('empresa'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('empresa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="asunto" class="col-md-4 col-form-label text-md-right">Asunto</label>

                            <div class="col-md-6">
                                <input id="asunto" type="text" class="form-control{{ $errors->has('asunto') ? ' is-invalid' : '' }}" name="asunto" value="{{ old('asunto') }}" required>

                                @if ($errors->has('asunto'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('asunto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mensaje" class="col-md-4 col-form-label text-md-right">Mensaje</label>

                            <div class="col-md-6">
                                <textarea name="mensaje" rows="10" cols="50" class="form-control{{ $errors->has('mensaje') ? ' is-invalid' : '' }}" required></textarea>

                                @if ($errors->has('mensaje'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mensaje') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Enviar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

