@extends('layouts.app')
@section('content')



<form id="regForm" method="POST" action="{{route('reclamo.store')}}" aria-label="">
  @csrf
    <div class="row">
        <div class="col text-center">
            <h2 class="h2-ente">RECLAMOS - CONSULTAS</h2>
            <br><br><br>
        </div>
    </div>
    <!-- One "tab" for each step in the form: -->
    <div class="tab">
      <p><input name="nro_cliente" placeholder="Número de Cliente (Obligatorio)" oninput="this.className = ''"></p>
      <p><input name="apellido_nombre" placeholder="Apellido y Nombre (Obligatorio)" oninput="this.className = ''"></p>
      <p><input name="telefono" placeholder="Teléfono de Cliente (Obligatorio)" oninput="this.className = ''"></p>
      <p><input name="email" id="email" placeholder="E-mail (Obligatorio)" oninput="this.className = ''"></p>
    </div>

    <div class="tab">
      <p><input name="calle" placeholder="Calle (Obligatorio)" oninput="this.className = ''"></p>
      <p><input name="barrio" placeholder="Barrio (Obligatorio)" oninput="this.className = ''"></p>
      <p><input name="localidad" placeholder="Localidad (Obligatorio)" oninput="this.className = ''"></p>
    </div>

    <div class="tab">
      <p>

          <label class="p-2"><input type="radio" name="tipo_reclamo" value="Reclamo" oninput="this.className = ''" checked>Reclamos </label>
          <label class="p-2"><input type="radio" name="tipo_reclamo" value="Consulta" oninput="this.className = ''">Consultas </label>
          <label class="p-2"><input type="radio" name="tipo_reclamo" value="Sugerencia" oninput="this.className = ''">Sugerencias </label>
          <label class="p-2"><input type="radio" name="tipo_reclamo" value="Felicitacion" oninput="this.className = ''">Felicitaciones </label>




        </p>


        <p>

          <label class="p-2" style="color:brown;">EDESA <input type="radio" name="empresa" value="EDESA" oninput="this.className = ''" checked></label>
        <label class="p-2" style="color: darkblue;"  > Aguas del Norte  <input type="radio" name="empresa" value="Aguas del Norte" oninput="this.className = ''"></label>

        </p>
      <p><input placeholder="Asunto (Obligatorio)" name="asunto" oninput="this.className = ''"></p>
      <textarea id="mensaje" name="mensaje" cols="125" rows="10"  placeholder="Mensaje (Obligatorio)"  oninput="this.className = ''" required></textarea>

    </div>



    <div style="overflow:auto;">
      <div style="float:right;">
        <button type="button" id="prevBtn" onclick="nextPrev(-1)">Volver</button>
        <button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
      </div>
    </div>

    <!-- Circles which indicates the steps of the form: -->
    <div style="text-align:center;margin-top:40px;">
      <span class="step"></span>
      <span class="step"></span>
      <span class="step"></span>

    </div>

    </form>

@push('scripts')
<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Enviar";
  } else {
    document.getElementById("nextBtn").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:

  if (currentTab==2 && document.getElementById("mensaje").value == ""){
      alert('el mensaje no puede estar vacio. ')
      valid = false;
    }


    if (currentTab==0){

        var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if  (! regex.test(document.getElementById("email").value)){
            valid = false;
            alert('El correo no es válido');
        }
    }


  for (i = 0; i < y.length; i++) {
    // If a field is empty...



    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }


  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
</script>
@endpush

@endsection

