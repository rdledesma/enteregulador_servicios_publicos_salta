<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <head profile="http://gmpg.org/xfn/11">

        <title>
            Ente Regulador&nbsp;|&nbsp;Organismo de Control de Agua y Energia de la Provincia de Salta							</title>

            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <link rel="stylesheet" type="text/css" href="http://www.entereguladorsalta.gov.ar/wp-content/themes/gazette/style.css" media="screen" />
            <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.entereguladorsalta.gov.ar/?feed=rss2" />
            <link rel="pingback" href="http://www.entereguladorsalta.gov.ar/xmlrpc.php" />
            <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
            <!--[if IE 6]>
            <script type="text/javascript" src="http://www.entereguladorsalta.gov.ar/wp-content/themes/gazette/includes/js/suckerfish.js"></script>
            <![endif]-->
             <!-- <script type="text/javascript" src="http://www.entereguladorsalta.gov.ar/wp-content/themes/gazette/wp-content/plugins/wp-menu-creator/external.js"></script>-->

                    <script type="text/javascript">//<![CDATA[
                    // Google Analytics for WordPress by Yoast v4.2.7 | http://yoast.com/wordpress/google-analytics/
                    var _gaq = _gaq || [];
                    _gaq.push(['_setAccount', 'UA-35899187-1']);
                                    _gaq.push(['_trackPageview']);
                    (function () {
                        var ga = document.createElement('script');
                        ga.type = 'text/javascript';
                        ga.async = true;
                        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(ga, s);
                    })();
                    //]]></script>
                    <link rel='stylesheet' id='shadowbox-css-css'  href='http://www.entereguladorsalta.gov.ar/wp-content/plugins/shadowbox-js/shadowbox/shadowbox.css?ver=3.0.3' type='text/css' media='screen' />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">


    <!-- Styles -->

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>




    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/app2.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <!------ Include the above in your HEAD tag ---------->
</head>
<body style="background-color: white;">
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light  navbar-ente">
            <a class="navbar-brand" href="#"><p class="p-ente" style="color: white;">Ente Regulador</p></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link p-ente p-4 mx-3" href="/" style="color: white;">INICIO</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link p-ente p-4 mx-3" href="{{route('reclamo.create')}}" style="color: white;">RECLAMOS SUGERENCIAS FELICITACIONES</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link p-ente p-4 mx-3" href="{{ route('factura') }}"style="color: white;">ENTENDÉ TU FACTURA</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link p-ente p-4 mx-3" href="{{ route('contacto') }}" style="color: white;">CONTACTO</a>
                    </li>
                </ul>

                <div class="form-inline pull-right p-2">
                    <a href="#"  data-toggle="modal" data-target="#menu" >
                        <i class="fab fa-facebook fa-2x fa-ente mx-3"></i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#menu" >
                        <i class="fab fa-twitter fa-2x fa-ente mx-3"></i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#menu">
                        <i class="fab fa-instagram fa-2x fa-ente mx-3"></i>
                    </a>
                    <a href="#"  data-toggle="modal" data-target="#menu">
                        <i class="fab fa-whatsapp fa-2x fa-ente mx-3"></i>
                    </a>
                </div>
            </div>


          </nav>


       <div class="row">
           <div class="col">
            <div class="col text-center" >

                <div class="card mx-auto" style="border:none; width: 25%; ">

                        <img
                      class="card-img-top"
                      src="{{URL::asset('/img/logo.png')}}"
                      alt="Card image cap"

                      >

                </div>


            </div>
           </div>

       </div>



</div>


    </div>

    <main class="container">
        @yield('content')
    </main>

    @include('layouts.partials.footer')





    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

@stack('scripts')
</body>
</html>
