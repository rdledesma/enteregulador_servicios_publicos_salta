<footer class="footer " style=" width: 100%; max-width: 100%;
  overflow-x: hidden;">

    <div class="row mx-5 d-flex justify-content-around ">
        <div class="col mx-5 m-5">
            <img src="{{URL::asset('/img/ente-blanco.png')}}"  width="200" alt="profile Pic" >
            <div class="row-sm">
                <div class="col-sm-6">
                  <p class="p-ente" style="color:white;">contacto@enteregulador.salta.gov.ar</p>
                  <p class="p-ente" style="color:white;">Salta Capital - CP:4400</p>
                  <p class="p-ente" style="color:white;">0800-4447-400 - Linea gratuita</p>
                  <p class="p-ente" style="color:white;">387 634-7400 - Whatsapp</p>

                </div>
                <div class="col-sm-6">
                  <p class="p-ente" style="color:white;">SUCURSALES</p>
                  <p class="p-ente" style="color:white;">Tartagal | Orán | Rosario de la Frontera</p>
                  <p class="p-ente" style="color:white;">Convenios Municipales</p>
                </div>
              </div>
        </div>
        <div class="col m-5">


            <div class="row pull-right">
                <div class="col-12">
                    <a href="#"  data-toggle="modal" data-target="#menu" >
                        <i class="fab fa-facebook fa-2x fa-ente"></i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#menu" >
                        <i class="fab fa-twitter fa-2x fa-ente"></i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#menu">
                        <i class="fab fa-instagram fa-2x fa-ente"></i>
                    </a>
                    <a href="#"  data-toggle="modal" data-target="#menu">
                        <i class="fab fa-whatsapp fa-2x fa-ente"></i>
                    </a>
                </div>

                <br>
                <br>

                <div class="row-12">
                    <div class="col-12">
                        <img src="{{URL::asset('/img/gobierno.png')}}"  width="200" alt="profile Pic" >
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col text-center">
          <p class="p-ente" style="color:white;"><a href = "mailto: lt.disenoycomunicacion@gmail.com">Desarrollado por L&T Diseño y Comunicación Email:lt.disenoycomunicacion@gmail.com</a></p>


        </div>
      </div>
</footer>
