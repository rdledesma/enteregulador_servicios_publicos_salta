@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col text-center">
                <h3 class="h3-ente">SUBSIDIOS</h3>
        </div>
</div>

<div class="row m-4 d-flex justify-content-between" >
        <div class="col-6">

                <div class="row">
                        <h5 class="h5-ente">¿Qué es un subsidio por indigencia?</h5>
                        <p class="p-ente text-justify">El subsidio por indigencia
                                a los servicios
                                públicos es un descuento en la
                                facturación de los servicios públicos de un 60% o 100%,
                                según el puntaje obtenido en la postulación.
                        </p>
                </div>

                <div class="row">
                        <h5 class="h5-ente">¿Cuál es el objetivo del subsidio?</h5>
                        <p class="p-ente text-justify">Asistir en ayuda de aquellos
                                usuarios que reciben los servicios y no se encuentran
                                en condiciones de pagar por ellos.
                        </p>
                </div>
                <div class="row">
                        <h5 class="h5-ente">¿Quién otorga los subsidios?</h5>
                        <p class="p-ente text-justify">El Estado Provincial otorga el Subsidio a los Servicios Sanitarios y/o Eléctricos, a través de este Ente o de las Concesionarias de Servicios Públicos en aquellas localidades donde no existen delegaciones del Organismo abocadas a la toma de postulaciones.

                        </p>
                </div>
                <div class="row">
                        <h5 class="h5-ente">¿Quíen recibe los subsidios?</h5>
                        <p class="p-ente text-justify">Usuarios que por su condición de carentes de recursos para satisfacer sus necesidades básicas esenciales, no podrían acceder a los servicios públicos sin la ayuda del Estado.

                        </p>
                </div>


                <div class="row">
                        <h5 class="h5-ente">¿Cuáles son las causas del rechazo?</h5>
                        <p class="p-ente text-justify">
                                Un subsidio puede salir rechazado debido a que dentro del grupo familiar algún miembro posea otro inmueble a su nombre, automóvil /motocicleta, o se tengan contratados servicios como televisión por cable o satelital, internet, teléfono, etc.
                        </p>
                </div>
                <div class="row">
                        <h5 class="h5-ente">Metodología de otorgamiento de subsidios</h5>
                        <p class="p-ente text-justify">
                                La postulación es personal e individual.
                                La asignación es directa, ya que es el mecanismo más transparente, eficiente y eficaz para identificar a las personas más necesitadas, mediante la carga de parámetros objetivos en relación a Ingresos, Patrimonio, Cargas Familiares y Vivienda.
                        </p>
                </div>

                <div class="row">
                        <h5 class="h5-ente">¿Cuánto dura un subsidio?</h5>
                        <p class="p-ente text-justify">
                                El plazo de duración establecido es de 12 meses. Luego de finalizado el período anual, el subsidio puede ser renovado realizando nuevamente el trámite.
                        </p>
                </div>
        </div>


        <div class="col-5 border ">
                <div class="row">
                        <div class="col text-center">
                                <h5 class="h5-ente">DESCARGAR</h5>
                        </div>
                </div>
                <a class="example-image-link" target="_blank" href="{{URL::asset('/img/folletos/RequisitosSubsidios.jpg')}}" data-lightbox="example-1">
                        <div class="row mt-5">

                                <div class="col">
                                        <h4 class="h5-ente" style="color:#990000;">Requisitos para subsidios a los servicios de agua y luz, otorgados por el Gobierno de la Provincia de Salta
                                        </h4>
                                </div>
                                <div class="col">
                                        <img class="example-image" src="{{URL::asset('/img/folletos/RequisitosSubsidios.jpg')}}"
                                        alt="Girl looking out people on beach" width="50%">
                                </div>
                        </div>
                </a>
                <a class="example-image-link" target="_blank" href="{{URL::asset('/img/folletos/RequisitosBenemetricas.jpg')}}" data-lightbox="example-1">
                        <div class="row mt-5">

                                <div class="col">
                                        <h4  class="h5-ente" style="color:#990000;">Requisitos para Instituciones Beneméritas</h4>
                                </div>
                                <div class="col">
                                        <img src="{{URL::asset('/img/folletos/RequisitosBenemetricas.jpg')}}" alt="profile Pic" width="50%" >
                                </div>
                        </div>
                </a>
                <a class="example-image-link" target="_blank" href="{{URL::asset('/img/folletos/RequisitosDeportivas.jpg')}}" data-lightbox="example-1">
                        <div class="row mt-5">

                                <div class="col">
                                        <h4 class="h5-ente" style="color:#990000;">Requisitos para Instituciones Deportivas</h4>
                                </div>
                                <div class="col">
                                        <img src="{{URL::asset('/img/folletos/RequisitosDeportivas.jpg')}}" alt="profile Pic" width="50%" >
                                </div>
                        </div>
                </a>
                <a class="example-image-link" target="_blank" href="{{URL::asset('/img/folletos/RequisitosElectro.jpg')}}" data-lightbox="example-1">
                        <div class="row mt-5">

                                <div class="col">
                                        <h4 class="h5-ente" style="color:#990000;">Requisitos para Electro dependientes</h4>
                                </div>
                                <div class="col">
                                        <img src="{{URL::asset('/img/folletos/RequisitosElectro.jpg')}}" alt="profile Pic" width="50%" >
                                </div>
                        </div>
                </a>





        </div>
</div>


@endsection
