@extends('layouts.app')

@section('content')


<div class="container-fluid text-center ml-2">
    <div class="row ml-5 mb-5" style="width: 110%;">
        <div class="col-lg-4">
            <div class="card" style="border:none; width: 50%;">
                <a href="{{route('reclamo.create')}}">
                    <img 
                  class="card-img-top" 
                  src="{{URL::asset('/img/reclamo-positivo.png')}}" 
                  alt="Card image cap"
                  id="reclamo-card"
                  >
                <div class="text-center">
                    <h5 class="card-title" style="color: #4d4d4d;">RECLAMOS</h5>
                </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4">
            <a href="{{route('contacto')}}">
                <div class="card"  style="border:none; width: 50%;">
                    <img class="card-img-top"
                      src="{{URL::asset('/img/contacto-positivo.png')}}" 
                      alt="Card image cap"
                      id="contacto-card">
                    <div class="text-center">
                        <h5 class="card-title" style="color: #4d4d4d;">CONTACTO</h5>
                    </div>
                  </div>
            </a>
        </div>

        <div class="col-lg-4">
            <a href="{{route('subsidio')}}">
                <div class="card"  style="border:none; width: 50%;">
                    <img 
                      class="card-img-top "
                      src="{{URL::asset('/img/subsidio-positivo.png')}}" 
                      alt="Card image cap"
                      id="subsidio-card"
                      >
                    <div class="text-center">
                        <h5 class="card-title" style="color: #4d4d4d;">SUBSIDIOS</h5>
                    </div>
                  </div>
            </a>
        </div>
        
        <div class="col-lg-4">
            <a href="https:wa.link/3awE">
                <div class="card"  style="border:none; width: 50%;">
                    <img 
                    class="card-img-top" 
                    src="{{URL::asset('/img/whapp-positivo.png')}}" 
                    alt="Card image cap"
                    id="whatsapp-card">
                    <div class="text-center">
                        <h5 class="card-title" style="color: #4d4d4d;">WHATSAPP</h5>
                    </div>
                  </div>
            </a>
            
        </div>


        <div class="col-lg-4">
            <a href="{{route('informacion')}}">
                <div class="card"  style="border:none; width: 50%;">
                    <img id="info-card" class="card-img-top" src="{{URL::asset('/img/info-positivo.png')}}" alt="Card image cap">
                    <div class="text-center">
                        <h5 class="card-title" style="color: #4d4d4d;">INFORMACIÓN</h5>
                    </div>
                  </div>
            </a>
        </div>


        <div class="col-lg-4">
            <a href="https://www.argentina.gob.ar/electrodependientes" target="_blank">
            <div class="card"  style="border:none; width: 50%;">
                
                  <img class="card-img-top" 
                    id="electro-card" 
                    src="{{URL::asset('/img/electro-positivo.png')}}" 
                    alt="Card image cap"
                    >
                
                <div class=" text-center">
                  <h5 class="card-title" style="color: #4d4d4d;">ELECTRODEPENDIENTES</h5>
                </div>
              </div>
            </a>
        </div>
    </div>

    <div class="row">
      <div class="col text-center">
          <h2 class="h2-ente">ENTE EDUCA</h2>
      </div>
    </div>


    <div class="row" style="background-color: #E6E6E6;">
        <div class="col text-center">
            <h2 class="h2-ente">NOTICIAS</h2>
        </div>
    
    <div class="row m-3" >
        @foreach ($noticias as $noticia)
        <div class="col-sm-3 mb-5">
            <a href="{{route('noticia.show',$noticia->id)}}">
              <div class="card" style=" border: black; background-image: url({{route('noticia.file',$noticia->id)}});  background-size: cover;   ">
                <div class="card-body text-left" style="background-color:black; opacity: 0.7">
                  <br><br><br>
                  <div class="row">
                      <div class="col">
                          <p style="display: -webkit-box;
                          -webkit-box-orient: vertical;
                          -webkit-line-clamp: 3;  /* Number of lines displayed before it truncate */
                           overflow: hidden;color:white;" class="card-text p-tally font-weight-bold">{{$noticia->titulo}}</p>
  
                      </div>
                  </div>      
                  <p class="card-text" style="color:white;"><small>{{ \Carbon\Carbon::parse($noticia->created_at)->format('d/m/Y')}}</small></p>
                </div>
              </div>
            </a>
        </div>
        @endforeach
        
      </div>
    

    </div>


    <div class="row center d-flex justify-content-center" >
        <div class="row text-center ">
            <div class="col">
              <h2 class="h2-ente">AUTORIDADES</h2>
            </div>
        </div>
        @include('partials.carusel')
    </div>
    
</div>


@push('scripts')
<script>
    $( document ).ready(function() {
    

   

    
    $("#reclamo-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/reclamo-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/reclamo-positivo.png')}}");
    }
    });


    $("#contacto-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/contacto-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/contacto-positivo.png')}}");
    }
    });

    
    $("#subsidio-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/subsidio-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/subsidio-positivo.png')}}");
    }
    });


    $("#whatsapp-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/whapp-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/whapp-positivo.png')}}");
    }
    });

    $("#info-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/info-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/info-positivo.png')}}");
    }
    });

    $("#electro-card").on({
     mouseenter: function(){
      $(this).attr('src',"{{URL::asset('/img/electro-negativo.png')}}");

    },
    mouseleave: function(){
      $(this).attr('src',"{{URL::asset('/img/electro-positivo.png')}}");
    }
    });
  });



 



</script>
@endpush
@endsection
