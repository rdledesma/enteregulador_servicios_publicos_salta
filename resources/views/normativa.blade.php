@extends('layouts.app')

@section('content')


<div class="row">
    <div class="col text-center">
        <h3 class="h2-ente">NORMATIVAS</h3>
    </div>
</div>

<div class="row">
    <div class="col">
        <h4 class="h3-ente" style="font-size: 20px;">MARCO REGULATORIO PARA LA PRESTACIÓN DE LOS SERVICIOS SANITARIOS DE LA PROVINCIA DE SALTA</h4>
        <a href="{{URL::asset('/res/MARCOREGULATORIO.pdf')}}" class="p-ente">Descargar</a>
    </div>
    <div class="col">
        <h4 class="h3-ente">LEYES</h4>
        <div class="row">
            <div class="col">
                Ley Nº6.835 Creación del Ente
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{URL::asset('/res/ley-de-creacion-del-ente.zip')}}">Descargar</a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                Ley Nº6.819 Marco Regulatorio Eléctrico
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{URL::asset('/res/ley-6819-marco-regulatorio-electrico.zip')}}">Descargar</a>
            </div>
        </div>
        @foreach ($leyes as $ley)
        <div class="row">
            <div class="col">

                <p class="p-ente">{{$ley->titulo}}</p>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{route('ley.file',$ley->id)}}" class="p-ente">Descargar</a>
            </div>
        </div>
        @endforeach

    </div>
</div>


<div class="row">
    <div class="col text-center">
        <h4 class="h3-ente">DECRETOS</h4>
    </div>
</div>
<div class="row d-flex justify-content-center">

    <div class="col-12">

        @foreach ($decretos as $item)
        <div class="row">
            <div class="col d-flex justify-content-start">
                <p class="p-ente">{{$item->titulo}}</p>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{route('decreto.file',$item->id)}}" class="p-ente">Descargar</a>
            </div>
        </div>
        @endforeach
    </div>
</div>

<div class="row">
    <div class="col text-center">
        <h4 class="h3-ente">REGLAMENTOS</h4>
    </div>
</div>
<div class="row d-flex justify-content-center">

    <div class="col-12">

        @foreach ($reglamentos as $item)
        <div class="row">
            <div class="col d-flex justify-content-start">
                <p class="p-ente">{{$item->titulo}}</p>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{route('reglamento.file',$item->id)}}" class="p-ente">Descargar</a>
            </div>
        </div>
        @endforeach
    </div>
</div>


<div class="row">
    <div class="col text-center">
        <h4 class="h3-ente">RESOLUCIONES</h4>
    </div>
</div>
<div class="row d-flex justify-content-center">

    <div class="col-12">

        @foreach ($resoluciones as $item)
        <div class="row">
            <div class="col d-flex justify-content-start">
                <p class="p-ente">{{$item->titulo}}</p>
            </div>
            <div class="col d-flex justify-content-end">
                <a href="{{route('reglamento.file',$item->id)}}" class="p-ente">Descargar</a>
            </div>
        </div>
        @endforeach
    </div>
</div>



@endsection
