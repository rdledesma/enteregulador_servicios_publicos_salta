<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">


    <!-- Styles -->

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>




    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/app2.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>






</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light  navbar-ente">
        <a class="navbar-brand" href="#"><p class="p-ente" style="color: white;">Ente Regulador</p></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                        <a class="nav-link p-ente p-4" href="/home" style="color: white;">Admin</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{route('noticia.admin')}}" style="color: white;">Noticias</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{ route('autoridad.admin') }}"style="color: white;">Autoridades</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{ route('ley.admin') }}"style="color: white;">Leyes</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{ route('decreto.admin') }}"style="color: white;">Decretos</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{ route('reglamento.admin') }}"style="color: white;">Reglamentos</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link p-ente p-4" href="{{ route('resolucion.admin') }}"style="color: white;">Resoluciones</a>
                  </li>


                  <li class="nav-item">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <a href="" class="btn btn-light"><button class="btn btn-outline-danger" type="submit">Logout</button></a>
                      </form>
                </li>
            </ul>
        </div>
      </nav>
    <div class="container">
        <main class="container">
            @yield('content')
        </main>
    </div>


</body>
</html>

