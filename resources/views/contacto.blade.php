@extends('layouts.app')
@section('content')
<div class="row">
        <div class="col text-center">
            <h3 class="h3-ente">CONTACTOS</h3>
        </div>
    </div>

<iframe class="w-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3622.6144689445537!2d-65.41186488499892!3d-24.774404384094577!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x941bc3c137896e8f%3A0x20e00880125d15ce!2sEnte%20regulador%20de%20Servicios%20P%C3%BAblicos!5e0!3m2!1ses-419!2sar!4v1580176865380!5m2!1ses-419!2sar"  height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


<br><br><br><br><br><br>

<div class="row d-flex justify-content-center">
        <div class="col-sm-3">
                <div class="col text-center">
                        <p><i class="fa fa-map-marker-alt fa-2x" style="color: #990000;"></i></p>
                    
                        <p class="p-ente-rojo" >Domicilio</p>
                        <p class="p-ente">Salta (A4400EIE)</p>
                        <p class="p-ente">Bartolomé Mitre 1231<p>
                    
                </div>
                <br>
                <div class="col text-center">
                        <i class="fa fa-mobile-alt fa-2x" style="color: #990000;"></i>
                                <p class="p-ente-rojo">Linea Gratuita</p>
                                <p class="p-ente">0800-444-7400</p>
                                <p class="p-ente-rojo">Conmutador</p>
                                <p class="p-ente">0387-4213021</p>
                    
                </div>


        </div>
        <div class="col-sm-5">
                <div class="col text-center">
                        <i class="far fa-envelope fa-2x" style="color: #990000;"></i>
                        
                                <p class="p-ente-rojo">Mail</p>
                                <p class="p-ente">institucionales@entereguladorsalta.gov.ar</p> 
                                <br><br>
                        
                    </div>
                    <br>
                    <div class="col text-center">
                        <i class="fa fa-globe fa-2x" style="color: #990000;"></i>
                        <p class="p-ente">www.entereguladorsalta.gov.ar</p>
                </div>
        </div>
</div>


<br><br><br><br><br><br>

<div class="row">
        <div class="col text-center">
            <h3 class="h4-ente">TARTAGAL</h3>
            <P class="text">España 113 </P>
            <P class="p-ente"> Horario de atención: 8 a 14 Hs.</P>
            <P class="p-ente"> Tel.: 03873-422-922</P>
            <P class="p-ente"> Mail: enresptartagal@entereguladorsalta.gov.ar</P>
        </div>

        <div class="col text-center">
                <h3>ROSARIO DE LA FRONTERA</h3>
                <P class="p-ente">Belgrano 476</P>
                <P class="p-ente"> Horario de atención: 8 a 14 Hs.</P>
                <P class="p-ente"> Tel.: 03876-483-782</P>
                <P class="p-ente"> Mail: enresptartagal@entereguladorsalta.gov.ar</P>
            </div>
    
        <div class="col text-center">
        <h3 class="h4-ente">ORÁN</h3>
        <P class="p-ente">Sarmiento 298 </P>
        <P class="p-ente"> Horario de atención: 8 a 14 Hs.</P>
        <P class="p-ente"> Tel.: 03873-422-922</P>
        <P class="p-ente"> Mail: enresptartagal@entereguladorsalta.gov.ar</P>
        </div>
</div>



<br><br><br><br>
<div class="row">
        <div class="col text-center">
            <h3 class="h3-ente">CONVENIOS MUNICIPALES</h3>
        </div>
</div>


<div class="row">
        <div class="col text-center">
            <h3 class="h4-ente">METÁN</h3>
            <P class="text-">Terminal - Alem Oeste Nº 56</P>
                <P class="p-ente"> Horario de atención: 8 a 14 Hs..</P>
                <P class="p-ente">Tel.: 03876-421-6322</P>
                <P class="p-ente">Mail: enrespmetan@entereguladorsalta.gov.ar</P>
        </div>

        <div class="col text-center">
                <h3 class="h4-ente">GÜEMES</h3>
                <P class="p-ente">Alberdi Nº 483</P>
                <P class="p-ente"> Horario de atención: 8 a 14 Hs..</P>
                <P class="p-ente"> Tel.: 0387-464-396</P>
                <P class="p-ente">Mail: enrespguemes@entereguladorsalta.gov.ar</P>
        </div>
</div>

@endsection