@extends('home')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
      <a href="{{route('autoridad.create')}}">Nueva Autoridad</a>
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>
            
                <th scope="col">Cargo</th>
                
                <th scope="col">Nombre</th>
                <th scope="col">Acción</th>

          </tr>
        </thead>
        <tbody>
            
            @foreach ($authorities as $authority)
            <tr>
                <td> {{$authority->cargo}}</td> 
                <td> {{$authority->nombre}}</td>
              

              
            
              
              
              <td>
                <form action="{{action('AuthorityController@destroy', $authority->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
              </td>

            </tr>
           
            

            @endforeach
          
                
        </tbody>
      </table>
    </div>
</div>

</div>

<br><br><br>

<br><br><br>
<br><br><br>

@endsection
