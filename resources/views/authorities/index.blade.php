@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
      <a href="{{route('autoridad.create')}}">Nuevo</a>
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>
            
                <th scope="col">Nombre</th>
                <th scope="col">Cargo</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Foto</th>

                <th scope="col">Acción</th>

          </tr>
        </thead>
        <tbody>
            
            @foreach ($authorities as $authority)
            <tr>
                
              <td> {{$authority->nombre}}</td>
              <td> {{$authority->cargo}}</td>
              <td> {{$authority->descripcion}}</td>
              <td> <img src="{{route('image.displayImage',$authority->id)}}" alt="Imagen no encontrada" width="15%"></td>
            
              
              
              <td>
                <form action="{{action('AuthorityController@destroy', $authority->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
              </td>

            </tr>
           
            

            @endforeach
          
                
        </tbody>
      </table>
    </div>
</div>

</div>


@endsection
