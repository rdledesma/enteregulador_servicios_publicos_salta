<div class="modal fade modal-slide-in-right" aria-hidem="true" role="dialog" tabindex="-1" id="modal-delete-{{$authority->id}}">


    <form action="{{action('AuthorityController@destroy', $authority->id)}}" method="post">
          @csrf
      <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="close">
                          <span aria-hidden="true">x</span>
                      </button>
                      <h4 class="modal-tittle">Eliminar Autoridad</h4>
                  </div>
                  <div class="modal-body">
                      <p>Confirme si desea eliminar {{$authority->nombre }}</p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      
                      <input name="_method" type="hidden" value="DELETE">
                      <button class="btn btn-danger" type="submit">Delete</button>
                  </div>
              </div>
          </div>

</form>

</div>