@extends('home')
@section('content')

<div class="container">
    <div class="row d-flex justify-content-center">
      <a href="{{route('ley.create')}}">Nueva Ley</a>
        <div class="col d-flex justify-content-center">
            <table class="table table-striped table-responsive text-center ">
                <thead>
            <tr>

                <th scope="col">Título</th>

                <th scope="col">Creado</th>
                <th scope="col">Acción</th>

          </tr>
        </thead>
        <tbody>

            @foreach ($laws as $law)
            <tr>
                <td> {{$law->titulo}}</td>
                <td> {{$law->created_at}}</td>






              <td>
                <form action="{{action('LawController@destroy', $law->id)}}" method="post">
                  @csrf
                  <input name="_method" type="hidden" value="DELETE">
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
              </td>

            </tr>



            @endforeach


        </tbody>
      </table>
    </div>
</div>

</div>

<br><br><br>

<br><br><br>
<br><br><br>

@endsection
